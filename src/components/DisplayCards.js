import React from "react";

class DisplayCards extends React.Component {
  componentDidMount() {
    const { value } = this.props.card;
    if (value === "QUEEN") {
     this.props.queenCounter()
    }
  }

  render() {
    const { image, value, suit, code } = this.props.card;
    return (
      <>
        <h1>
          {value} of {suit}
        </h1>
        <img src={image} alt="card" />
      </>
    );
  }
}

export default DisplayCards;
