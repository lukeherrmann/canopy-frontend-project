import React from "react";
import DisplayCards from "./components/DisplayCards";
import axios from "axios";
import "./styles/main.sass";

class App extends React.Component {
  state = { deckId: "", cards: [], queenCounter: 0 };

  componentDidMount() {
    this.getNewDeck();
  }

  getNewDeck = async () => {
    let res = await axios.get(
      `https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1`
    );
    let deckId = res.data.deck_id;
    this.setState({ deckId: deckId });
    debugger;
    this.getNewCards();
  };

  getNewCards = async () => {
    let res = await axios.get(
      `https://deckofcardsapi.com/api/deck/${this.state.deckId}/draw/?count=2`
    );
    this.setState({ cards: [...res.data.cards]})

  };

  updateQueenCounter = () => {
    this.setState({ queenCounter: (this.state.queenCounter += 1) });
  };

  startGame = () => {
    for (let i = 0; i <= 25; i++) {
      setTimeout(this.getNewCards, 5000);
    }
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-offset-5">
            <button className="start-button">
              <h1 onClick={this.startGame}>Start Card Game</h1>
            </button>
            <h1>Queen Counter: {this.state.queenCounter}</h1>
          </div>
        </div>
        <div className="row">
          {this.state.cards.map(card => {
            return (
              <div className="col-sm">
                <DisplayCards
                  card={card}
                  queenCounter={this.updateQueenCounter}
                />
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
export default App;
